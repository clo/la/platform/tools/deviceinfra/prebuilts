// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["deviceinfra_license"],
}

license {
    name: "deviceinfra_license",
    visibility: [":__subpackages__"],
    license_kinds: [
        "SPDX-license-identifier-Apache-2.0",
    ],
    license_text: [
        "LICENSE",
    ],
}

filegroup {
    name: "ats_console_prebuilt",
    srcs: [
        "ats_console_deploy.jar",
    ],
}

filegroup {
    name: "ats_olc_server_local_mode_prebuilt",
    srcs: [
        "ats_olc_server_local_mode_deploy.jar",
    ],
}

filegroup {
    name: "ats_olc_server_prebuilt",
    srcs: [
        "ats_olc_server_deploy.jar",
    ],
}

java_import_host {
    name: "ats_console_deploy",
    jars: [":ats_console_prebuilt"],
    installable: true,
}

java_import_host {
    name: "ats_olc_server_deploy",
    jars: [":ats_olc_server_prebuilt"],
    installable: true,
}

java_import_host {
    name: "ats_olc_server_local_mode_deploy",
    jars: [":ats_olc_server_local_mode_prebuilt"],
    installable: true,
}
